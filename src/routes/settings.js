var express = require('express');
var router = express.Router();


/* GET settings listing. */

router.get('/', async function(req, res, next) {

  res.json({ 
    temperatureSet: req.app.locals.temperatureSet
  });
});


router.post('/', async function(req, res, next) {

  console.log(req.body);

  res.setHeader('Content-Type', 'application/json');

  req.app.locals.temperatureSet = req.body.temperatureSet;

  const heat = await req.app.locals.checkTemperature();
  // console.log(heat);

  res.json({ 
    temperatureSet: req.app.locals.temperatureSet,
    heat: heat
  });
});


router.post('/heat', async function(req, res, next) {

  console.log(req.body);

  req.app.locals.heat = req.body.state;

  let code = 0;
  if (req.body.state) {
    code = 1;
  } else {
    code = 0;
  }

  req.app.locals.rfEmitter.sendCode(code, {pin: 0})
    .then(function(stdout) {
      console.log('Code sent: ', stdout);
      res.json({ 
        heat: req.app.locals.heat
      });
      next();
    }, function(error) {
      console.error('Code was not sent, reason: ', error);
      res.sendStatus(error);
    });
	
});


router.post('/heat/manual', async function(req, res, next) {

  console.log(req.body);

  res.setHeader('Content-Type', 'application/json');

  req.app.locals.manualHeat = req.body.manual;

  res.json({ 
    manual: req.app.locals.manualHeat
  });
});

module.exports = router;
