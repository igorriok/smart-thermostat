var express = require('express');
var router = express.Router();


router.get('/', function(req, res, next) {
  res.json({
    heat: req.app.locals.heat,
    manual: req.app.locals.manualHeat
  });
});

module.exports = router;
